require_relative './model/omdb'

title = ARGV[0]
OMDB_API_KEY = ENV['OMDB_API_KEY']

if title.nil?
  puts 'error: required movie title'
  exit 1
end

movie = OMDb.new(OMDB_API_KEY).search_by_title(title)
ratings = movie.ratings.map { |rating| "#{rating.source}: #{rating.value}" }.join(', ')

puts "Title: #{movie.title}\nYear: #{movie.year}\nDirector: #{movie.director}\nRatings: #{ratings}"
