class Movie
  attr_reader :title, :year, :director, :ratings

  def initialize(title, year, director, ratings)
    @title = title
    @year = year
    @director = director
    @ratings = ratings
  end
end
