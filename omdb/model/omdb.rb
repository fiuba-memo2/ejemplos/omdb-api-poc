require 'faraday'
require 'json'
require_relative './movie'
require_relative './rating'

class OMDb
  API_URL = 'http://www.omdbapi.com/'.freeze

  def initialize(api_key)
    @api_key = api_key
  end

  def search_by_title(title)
    response = Faraday.get(API_URL, { t: title, apikey: @api_key })
    response_json = JSON.parse(response.body)
    ratings = response_json['Ratings'].map do |rating|
      Rating.new(rating['Source'], rating['Value'])
    end
    Movie.new(response_json['Title'], response_json['Year'], response_json['Director'], ratings)
  end
end
