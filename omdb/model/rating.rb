class Rating
  attr_reader :source, :value

  def initialize(source, value)
    @source = source
    @value = value
  end
end
