# OMDb API proof of concept

## Correr la aplicación

```bash
$ OMDB_API_KEY={api_key} ruby app.rb {movie_title}
```

## Obtener la API Key

Visitar [OMDb API Key](http://www.omdbapi.com/apikey.aspx) y registrarse con account type free.

## Ejemplo

```bash
$ OMDB_API_KEY=123456 ruby app.rb titanic
```

Resultado:
```
Title: Titanic
Year: 1997
Director: James Cameron
Ratings: Internet Movie Database: 7.9/10, Rotten Tomatoes: 88%, Metacritic: 75/100
```
